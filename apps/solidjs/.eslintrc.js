module.exports = {
  extends: ['@solid/eslint-config-base'],
  rules: {
    'import/no-default-export': 'off',
    '@typescript-eslint/naming-convention': [
      'error',
      { selector: 'variableLike', format: ['camelCase', 'PascalCase'] }
    ],
    'import/no-unresolved': 'off',
    '@typescript-eslint/no-unsafe-assignment': 'off'
  }
};
