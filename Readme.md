# Monorepo Template with Rush + pnpm

## Descriptions
This repository is using RushJS to manage a monorepo repository, together with pnpm allow us to have a really fast and structure repository.

If you want to know more visit their webpage for more information [RushJs](www.rushjs.io) or [PNPM](www.pnpm.io)

## Requirements

In order to use this repo you will need to install 2 libraries, please see below the commands:

``npm install -g @microsoft/rush``
``npm install -g pnpm``


## Goal
Have a common template to use when we want to start new projects. Right now we are starting projects using commands like create-react-app or copying codes from other repos and removing things. 

The idea here is to make a basic starting point project with all the basic rules, standards and guidelines that will save us time.

## Stack

This project has the features below:

- Eslint
- Prettier
- Pre-commit validators
- rush releases

## How to use it?

To add a new project please add a folder under apps if you want to make a new website ie ``apps/mynewapp``. If you are trying to build a library please add a folder under libs folders ie ``libs/mynewlib``, as you see we want to follow a pattern grouping packages in categories, below you can see the categories allowed:

- apps
- libs
- widgets
- tests
- clis
- docs

### Adding a new project
To add a new project just add a folder under the category desire with your prefer framework. For example create-react-app. 

After this you will need to modify the file ``rush.json`` that is located in the root of this repository, search for the property **projects** and add the project information of your new project. 

After that you will need to run the command ``rush update``

Congratulations you have add a new project to the repo.

### how to run it?
You can use the custom command we already provide for you:

``rush dev``

this will go to each project folder and execute the command ``dev`` that must be define in their ``package.json``

### how to buil it?
You can use the **rush** commmand build. Just execute the command ```rush build```. This will go to each project folder and run the ``build`` command that must be define in their ``package.json``.

### how to add a new dependecy?
We are using pnpm that is an alternative to npm, this new package is extremely fast compare to yarn or npm. Initially this is a testing for pnpm to see if we can use it in our production projects. For now, if you need to add a dependency you will need to run the pnpm command: ```pnpm add package-name``

## Road Map
This is just a basic boilerplate, below you can see what features could be added in the future:

1. semantic commits
2. deploying scripts
3. releasing scripts
4. CI scripts
